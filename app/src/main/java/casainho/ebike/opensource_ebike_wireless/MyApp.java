package casainho.ebike.opensource_ebike_wireless;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import casainho.ebike.opensource_ebike_wireless.activities.BluetoothSetupActivity;
import casainho.ebike.opensource_ebike_wireless.data.LogManager;

import static casainho.ebike.opensource_ebike_wireless.activities.InitialTestActivity.TEST_RESULTS;

public class MyApp extends Application {
    private static MyApp instance;
    private static LogManager mLogManager;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mLogManager = LogManager.initLogs();
        PreferenceManager.getDefaultSharedPreferences(instance).edit().remove("VARIABLES").apply();
        //PreferenceManager.getDefaultSharedPreferences(instance).edit().remove("BodyInfo").apply();
        //PreferenceManager.getDefaultSharedPreferences(instance).edit().remove(TEST_RESULTS).apply();
    }

    public static MyApp getInstance() { return instance; }

    public static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(instance);
    }

    public static LogManager getLogManager() {
        return mLogManager;
    }
}
