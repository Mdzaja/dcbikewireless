package casainho.ebike.opensource_ebike_wireless;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import casainho.ebike.opensource_ebike_wireless.activities.InitialTestActivity;
import casainho.ebike.opensource_ebike_wireless.data.Global;
import casainho.ebike.opensource_ebike_wireless.data.TSDZ_Periodic;
import polar.com.sdk.api.PolarBleApi;
import polar.com.sdk.api.PolarBleApiCallback;
import polar.com.sdk.api.PolarBleApiDefaultImpl;
import polar.com.sdk.api.model.PolarDeviceInfo;
import polar.com.sdk.api.model.PolarHrData;

public class HRSensorService {

    private static HRSensorService hrSensorService = null;
    private static final String TAG = "HRSensorService";
    public static final String KEY_HR_SENSOR_ID = "HRSensorId";
    public static final String KEY_HR_SENSOR_NAME = "HRSensorName";

    private Context context = MyApp.getInstance().getApplicationContext();
    private boolean isConnected = false;
    private final TSDZ_Periodic mPeriodic = Global.getInstance().TSZD2Periodic;
    public PolarBleApi api;
    private Runnable task;
    public int batteryLevel;

    private HRSensorService() {
        api = PolarBleApiDefaultImpl.defaultImplementation(context,
                PolarBleApi.FEATURE_BATTERY_INFO |
                        PolarBleApi.FEATURE_HR);

        api.setApiCallback(new PolarBleApiCallback() {
            @Override
            public void blePowerStateChanged(boolean b) {
                Log.d(TAG, "BluetoothStateChanged " + b);
            }

            @Override
            public void deviceConnected(@NonNull PolarDeviceInfo s) {
                Log.d(TAG, "Device connected " + s.deviceId);
                Toast.makeText(context, "HR sensor connected.",
                        Toast.LENGTH_SHORT).show();

                SharedPreferences.Editor editor = MyApp.getPreferences().edit();
                editor.putString(KEY_HR_SENSOR_ID, s.deviceId);
                editor.putString(KEY_HR_SENSOR_NAME, s.name);
                editor.apply();

                isConnected = true;
            }

            @Override
            public void deviceConnecting(@NonNull PolarDeviceInfo polarDeviceInfo) {
                new Thread(() -> {
                    try {
                        Thread.sleep(5000);
                        if(!isConnected)
                            Toast.makeText(context, "Couldn\'t connect to selected HR sensor.",
                                    Toast.LENGTH_SHORT).show();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }).start();
            }

            @Override
            public void deviceDisconnected(@NonNull PolarDeviceInfo s) {
                Log.d(TAG, "Device disconnected " + s);

                SharedPreferences.Editor editor = MyApp.getPreferences().edit();
                editor.remove(KEY_HR_SENSOR_ID);
                editor.remove(KEY_HR_SENSOR_NAME);
                editor.apply();

                isConnected = false;
            }

            @Override
            public void hrFeatureReady(@NonNull String s) {
                Log.d(TAG, "HR Feature ready " + s);
            }

            @Override
            public void batteryLevelReceived(@NonNull String s, int i) {
                String msg = "ID: " + s + "\nBattery level: " + i;
                Log.d(TAG, "Battery level " + s + " " + i);
                batteryLevel = i;
                if(task != null) {
                    try{
                        task.run();
                    }catch (Exception e) {
                        Log.d("Error!!!", e.toString());
                    }
                }
            }

            @Override
            public void hrNotificationReceived(@NonNull String s,
                                               @NonNull PolarHrData polarHrData) {
                Log.d(TAG, "HR " + polarHrData.hr);
                mPeriodic.heartRate = polarHrData.hr;
                if(task != null) {
                    try{
                        task.run();
                    }catch (Exception e) {
                        Log.d("Error!!!", e.toString());
                    }
                }

            }
        });
    }

    public static HRSensorService getHRSensorService() {
        if(hrSensorService == null) hrSensorService = new HRSensorService();
        return hrSensorService;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setOnHRNotificationReceivedListener(Runnable task) {
        this.task = task;
    }
}
