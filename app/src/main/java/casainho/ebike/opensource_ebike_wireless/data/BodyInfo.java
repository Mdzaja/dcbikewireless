package casainho.ebike.opensource_ebike_wireless.data;

public class BodyInfo {
    public float weight;
    public float height;

    public BodyInfo() {
        weight = 0;
        height = 0;
    }

    public BodyInfo(float weight, float height) {
        this.weight = weight;
        this.height = height;
    }
}
