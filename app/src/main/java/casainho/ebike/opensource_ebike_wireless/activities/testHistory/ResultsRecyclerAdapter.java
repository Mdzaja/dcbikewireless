package casainho.ebike.opensource_ebike_wireless.activities.testHistory;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import casainho.ebike.opensource_ebike_wireless.R;
import casainho.ebike.opensource_ebike_wireless.data.InitialTestData;

public class ResultsRecyclerAdapter  extends RecyclerView.Adapter<ResultsRecyclerAdapter.ViewHolder> {

    private List<InitialTestData> results;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView maxHearthRateTV;
        public TextView maxPowerTV;
        public TextView maxTimeTV;

        public ViewHolder(View view) {
            super(view);

            maxHearthRateTV = view.findViewById(R.id.maxHearthRateTV);
            maxPowerTV = view.findViewById(R.id.maxPowerTV);
            maxTimeTV = view.findViewById(R.id.maxTimeTV);
        }
    }

    public ResultsRecyclerAdapter(List<InitialTestData> results) {
        this.results = results;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_test_result_layout, viewGroup, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        InitialTestData testData = results.get(position);

        viewHolder.maxHearthRateTV.setText(String.valueOf(testData.maxHearthRate));
        viewHolder.maxPowerTV.setText(String.valueOf(testData.maxPower));
        viewHolder.maxTimeTV.setText(String.valueOf(testData.maxTime));
    }

    @Override
    public int getItemCount() {
        return results.size();
    }
}
