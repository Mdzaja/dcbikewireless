package casainho.ebike.opensource_ebike_wireless.activities;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.webkit.WebView;
import casainho.ebike.opensource_ebike_wireless.R;

public class AboutActivity extends AppCompatActivity {

    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String versionName = "";
        PackageInfo pInfo;
        {
            try {
                pInfo = getApplicationContext().getPackageManager().getPackageInfo(getPackageName(), 0);
                versionName = pInfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }

        String content = "<h1>DC Bike Wireless</h1>\n" +
                "<p>Version: " + versionName + "\n" +
                "<p><br>\n" +
                "<img src=\"file:///android_asset/ic_bike_logo_color.jpg\" alt=\"Bike logo\" width=\"250\" height=\"200\">";

        // init webView
        webView = (WebView) findViewById(R.id.simpleWebView);
        // displaying text in WebView
        webView.loadDataWithBaseURL(null, content, "text/html", "utf-8", null);
    }
}