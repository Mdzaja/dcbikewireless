package casainho.ebike.opensource_ebike_wireless.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.MutableLiveData;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.nfc.FormatException;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import casainho.ebike.opensource_ebike_wireless.HRSensorService;
import casainho.ebike.opensource_ebike_wireless.MyApp;
import casainho.ebike.opensource_ebike_wireless.R;
import casainho.ebike.opensource_ebike_wireless.TSDZBTService;
import casainho.ebike.opensource_ebike_wireless.data.BodyInfo;
import casainho.ebike.opensource_ebike_wireless.data.Global;
import casainho.ebike.opensource_ebike_wireless.data.InitialTestData;
import casainho.ebike.opensource_ebike_wireless.data.TSDZ_Periodic;
import casainho.ebike.opensource_ebike_wireless.fragments.MyFragmentListener;

public class InitialTestActivity extends AppCompatActivity implements View.OnLongClickListener, MyFragmentListener {

    private final Gson gson = new Gson();
    private static final String TAG = "FragmentInitialTest";
    public static final int MINIMAL_PEDAL_CADENCE = 60;
    public static final int OPTIMAL_PEDAL_CADENCE = 90;
    public static final String TEST_RESULTS = "TestResults";

    private final TSDZ_Periodic periodic = Global.getInstance().TSZD2Periodic;
    private InitialTestData testData = new InitialTestData();
    private int power;

    private final IntentFilter mIntentFilter = new IntentFilter();

    private MutableLiveData<Integer> time = new MutableLiveData<>(0);
    private MutableLiveData<Integer> pedalCadence = new MutableLiveData<>(periodic.pedalCadence);

    TextView mBatterySOCTV,
            mHRSensorBatteryTV,
            mBrakeLightsTV,
            mPowerValueTV,
            mPedalCadenceValueTv,
            mMinimalPedalCadenceValueTv,
            mOptimalPedalCadenceValueTv,
            mTimeValueTV,
            mHeartRateTV,
            mSpeedTV;
    private Button startButton,
                   infoButton;

    SharedPreferences mPreferences  = MyApp.getPreferences();
    private final HRSensorService hrSensorService = HRSensorService.getHRSensorService();

    private boolean stopwatchRunning = false, collectData = false;
    ExecutorService executor = Executors.newSingleThreadExecutor();
    private Runnable stopwatchTask = new Runnable() {
        @Override
        public void run() {
            while(stopwatchRunning) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if(stopwatchRunning)
                    runOnUiThread(() -> time.setValue(time.getValue() + 1));
            }
        }
    };

    private float weight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_test);
        mIntentFilter.addAction(TSDZBTService.TSDZ_PERIODIC_WRITE_BROADCAST);

        setViews();
        setStartButton();
        setBodyInfoButton();
        time.observe(this, changedValue -> {
            if(stopwatchRunning && !collectData && changedValue >= 0) {
                collectData = true;
            }
            if(stopwatchRunning && changedValue > 0 && changedValue % 5/*promini posli*/ == 0) {
                power += weight < 90 ? 20 : 25;
                adjustPower();
                updateView();//TODO mozda nece tribat posli
            }
        });
    }

    @Override
    public void onBackPressed() {
        //TODO mozda ima neki loophole sta triba provjerit
        stopwatchRunning = false;
        super.onBackPressed();
    }

    public void setViews() {
        startButton = findViewById(R.id.startButton);
        infoButton = findViewById(R.id.bodyInfoButton);

        mBatterySOCTV = (TextView) findViewById(R.id.batterySOCTV);
        mBatterySOCTV.setText(String.valueOf(periodic.batterySOC / 10));

        mHRSensorBatteryTV = (TextView) findViewById(R.id.hrSensorBatteryTV);
        mHRSensorBatteryTV.setText(String.valueOf(hrSensorService.batteryLevel));

        mBrakeLightsTV = (TextView) findViewById(R.id.brakeLightsTV);
        mBrakeLightsTV.setText("");

        ((TextView) findViewById(R.id.powerTV)).setText(R.string.power);
        mPowerValueTV = (TextView) findViewById(R.id.powerValueTV);
        mPowerValueTV.setText(String.valueOf(power));

        ((TextView) findViewById(R.id.fl2TV)).setText(R.string.pedal_cadence);
        mPedalCadenceValueTv = (TextView) findViewById(R.id.fl2ValueTV);
        mPedalCadenceValueTv.setText(String.valueOf(periodic.pedalCadence));

        mMinimalPedalCadenceValueTv = (TextView) findViewById(R.id.minimalCadenceValueTV);
        mMinimalPedalCadenceValueTv.setText(String.valueOf(MINIMAL_PEDAL_CADENCE));

        mOptimalPedalCadenceValueTv = (TextView) findViewById(R.id.optimalCadenceValueTV);
        mOptimalPedalCadenceValueTv.setText(String.valueOf(OPTIMAL_PEDAL_CADENCE));

        ((TextView) findViewById(R.id.fl3TV)).setText("Time");
        mTimeValueTV = (TextView) findViewById(R.id.fl3ValueTV);
        time.observe(this, changedValue -> {
            StringBuilder sb = new StringBuilder();
            if(changedValue < 0) sb.append("-");
            sb.append(changedValue / 60);
            sb.append(":");
            sb.append(Math.abs(changedValue % 60));
            mTimeValueTV.setText(sb.toString());
        });

        ((TextView) findViewById(R.id.fl41TV)).setText(R.string.hearth_rate);
        mHeartRateTV = (TextView) findViewById(R.id.fl41ValueTV);
        mHeartRateTV.setText(String.valueOf(periodic.heartRate));

        ((TextView) findViewById(R.id.fl42TV)).setText(R.string.speed);
        mSpeedTV = (TextView) findViewById(R.id.fl42ValueTV);
        mSpeedTV.setText(String.valueOf(periodic.wheelSpeed));
    }

    @Override
    public void onResume() {
        super.onResume();
        updateView();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, mIntentFilter);
        hrSensorService.setOnHRNotificationReceivedListener(() -> updateView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive " + intent.getAction());
            if (intent.getAction() == null)
                return;
            switch (intent.getAction()) {
                case TSDZBTService.TSDZ_PERIODIC_WRITE_BROADCAST:
                    updateView();
                    break;
            }
        }
    };

    void updateView() {
        mBatterySOCTV.setText(String.valueOf((int) periodic.batterySOC));
        mHRSensorBatteryTV.setText(String.valueOf(hrSensorService.batteryLevel));

        if ((periodic.braking == 1) && (periodic.light == 1))
            mBrakeLightsTV.setText("B L");
        else if (periodic.braking == 1)
            mBrakeLightsTV.setText("B");
        else if (periodic.light == 1)
            mBrakeLightsTV.setText("L");
        else
            mBrakeLightsTV.setText("");

        mPowerValueTV.setText(String.valueOf(power));
        mPedalCadenceValueTv.setText(String.valueOf(periodic.pedalCadence));
        mSpeedTV.setText(String.valueOf(periodic.wheelSpeed));
        mHeartRateTV.setText(String.valueOf(periodic.heartRate));
        if(collectData) collectData();
    }

    @Override
    public void refreshView() {
        updateView();
    }

    @Override
    public boolean onLongClick(View v) {
        return false;
    }

    private void setStartButton() {
        startButton.setOnClickListener(b -> {
            String bodyInfoStr = mPreferences.getString("BodyInfo", "Oops");
            if(bodyInfoStr.equals("Oops")) {
                Toast.makeText(this, "Please enter your body information before starting test.", Toast.LENGTH_SHORT).show();
                return;
            }
            Gson gson = new Gson();
            weight = gson.fromJson(bodyInfoStr, BodyInfo.class).weight;

            startButton.setClickable(false);
            infoButton.setClickable(false);

            time.setValue(-10);
            power = 50;
            updateView();//TODO mozda nece tribat posli
            stopwatchRunning = true;
            executor.execute(stopwatchTask);
        });
    }

    private void collectData() {
        if(periodic.pedalCadence >= MINIMAL_PEDAL_CADENCE) {
            if(testData.maxHearthRate < periodic.heartRate)
                testData.maxHearthRate = periodic.heartRate;
        } else {
            endTest();
        }
    }

    private void endTest() {
        stopwatchRunning = false;
        collectData = false;
        startButton.setClickable(true);
        infoButton.setClickable(true);

        testData.maxPower = power;
        testData.maxTime = time.getValue();
        time.setValue(0);
        power = 0;
        updateView();//TODO mozda nece tribat posli

        //TODO izracunat podatke po formulama

        showTestResultDialog();
    }

    private void setBodyInfoButton() {
        infoButton.setOnClickListener(b -> {
            View dialogView = getLayoutInflater().inflate(R.layout.dialog_body_info, null);
            EditText weightET = dialogView.findViewById(R.id.weightEditText);
            EditText heightET = dialogView.findViewById(R.id.heightEditText);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Enter your body information")
                    .setView(dialogView)
                    .setPositiveButton("Save", (dialog, id) -> {  })
                    .setNegativeButton("Cancel", (dialog, id) -> {  });

            String bodyInfoStr = mPreferences.getString("BodyInfo", "Oops");
            if(!bodyInfoStr.equals("Oops")) {
                BodyInfo bodyInfo = gson.fromJson(bodyInfoStr, BodyInfo.class);
                weightET.setText(String.valueOf(bodyInfo.weight));
                heightET.setText(String.valueOf(bodyInfo.height));
            }

            AlertDialog dialog = builder.create();
            dialog.show();

            Button posButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
            posButton.setOnClickListener(l -> {
                Toast wrongFormatToast = Toast.makeText(this, "You must enter numbers greater than 0. Please try again.", Toast.LENGTH_SHORT);

                float weight, height;
                try {
                    weight = Float.parseFloat(weightET.getText().toString());
                    height = Float.parseFloat(heightET.getText().toString());
                    if(weight <= 0.001 || height <= 0.001)
                        throw new FormatException();
                } catch (Exception ex) {
                    wrongFormatToast.show();
                    return;
                }

                String updatedBodyInfoStr = gson.toJson(new BodyInfo(weight, height));
                if(!bodyInfoStr.equals(updatedBodyInfoStr))
                    mPreferences.edit().putString("BodyInfo", updatedBodyInfoStr).commit();
                dialog.dismiss();
            });
        });
    }

    private void adjustPower() {
        //TODO Andro
        //new CFG
        //readCFG
        //promini neke varijable u CFG
        //writeCFG
        //korisiti varijablu power
    }

    private void showTestResultDialog() {
        View dialogView = getLayoutInflater().inflate(R.layout.dialog_test_result, null);

        View linearLayout = dialogView.findViewById(R.id.resultLinLayout);
        linearLayout.setBackgroundColor(Color.WHITE);

        CardView cardView = dialogView.findViewById(R.id.resultCardView);
        cardView.setRadius(0);

        TextView maxHearthRateTV = dialogView.findViewById(R.id.maxHearthRateTV);
        TextView maxPowerTV = dialogView.findViewById(R.id.maxPowerTV);
        TextView maxTimeTV = dialogView.findViewById(R.id.maxTimeTV);

        maxHearthRateTV.setText(String.valueOf(testData.maxHearthRate));
        maxPowerTV.setText(String.valueOf(testData.maxPower));
        maxTimeTV.setText(String.valueOf(testData.maxTime));

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Test Result")
                .setView(dialogView)
                .setPositiveButton("Save", (dialog, id) -> saveTestResult() )
                .setNegativeButton("Discard", (dialog, id) -> {  });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void saveTestResult() {
        String resultsHistoryStr = mPreferences.getString(TEST_RESULTS, null);
        List<InitialTestData> resultsHistory = resultsHistoryStr == null ? new ArrayList<>() : new ArrayList<>(Arrays.asList(gson.fromJson(resultsHistoryStr, InitialTestData[].class)));
        resultsHistory.add(0, testData);

        if(resultsHistory.size() > 10)
            resultsHistory.remove(resultsHistory.size() - 1);

        resultsHistoryStr = gson.toJson(resultsHistory);
        mPreferences.edit().putString(TEST_RESULTS, resultsHistoryStr).commit();
    }
}