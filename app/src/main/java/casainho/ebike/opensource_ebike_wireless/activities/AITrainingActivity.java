package casainho.ebike.opensource_ebike_wireless.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

import casainho.ebike.opensource_ebike_wireless.HRSensorService;
import casainho.ebike.opensource_ebike_wireless.MyApp;
import casainho.ebike.opensource_ebike_wireless.R;
import casainho.ebike.opensource_ebike_wireless.TSDZBTService;
import casainho.ebike.opensource_ebike_wireless.data.Global;
import casainho.ebike.opensource_ebike_wireless.data.TSDZ_Periodic;
import casainho.ebike.opensource_ebike_wireless.data.Variable;

public class AITrainingActivity extends AppCompatActivity {

    private static final String TAG = "AITrainingActivity";

    private final TSDZ_Periodic periodic = Global.getInstance().TSZD2Periodic;
    private final HRSensorService hrSensorService = HRSensorService.getHRSensorService();
    private final SharedPreferences mPreferences  = MyApp.getPreferences();
    private final IntentFilter mIntentFilter = new IntentFilter();

    private TextView mBatterySOCTV,
            mHRSensorBatteryTV,
            mBrakeLightsTV,
            mAssistLevelValueTV;

    private View onVariableLongClickView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ai_training);
        mIntentFilter.addAction(TSDZBTService.TSDZ_PERIODIC_WRITE_BROADCAST);

        mBatterySOCTV = (TextView) findViewById(R.id.batterySOCTV);
        mBatterySOCTV.setText(String.valueOf(periodic.batterySOC / 10));

        mHRSensorBatteryTV = (TextView) findViewById(R.id.hrSensorBatteryTV);
        mHRSensorBatteryTV.setText(String.valueOf(hrSensorService.batteryLevel));

        mBrakeLightsTV = (TextView) findViewById(R.id.brakeLightsTV);
        mBrakeLightsTV.setText("");

        ((TextView) findViewById(R.id.assistLevelTV)).setText(R.string.assist_level);
        mAssistLevelValueTV = (TextView) findViewById(R.id.assistLevelValueTV);
        mAssistLevelValueTV.setText(String.valueOf(periodic.assistLevel));

        findViewById(R.id.fl1).setOnLongClickListener(this::longClickSelectVariable);
        findViewById(R.id.fl31).setOnLongClickListener(this::longClickSelectVariable);
        findViewById(R.id.fl32).setOnLongClickListener(this::longClickSelectVariable);
        findViewById(R.id.fl41).setOnLongClickListener(this::longClickSelectVariable);
        findViewById(R.id.fl42).setOnLongClickListener(this::longClickSelectVariable);

        boolean resetVariables = false;
        if (resetVariables == false) {
            Gson gson = new Gson();
            String storedHashMapString = mPreferences.getString("VARIABLES", "oopsDintWork");

            if (storedHashMapString.contains("oopsDintWork")) {
                resetVariables = true;
            } else {
                Type type = new TypeToken<HashMap<Integer, Variable>>(){}.getType();
                periodic.variablesConfig = gson.fromJson(storedHashMapString, type);
            }
        }

        if (resetVariables || periodic.variablesConfig == null || periodic.variablesConfig.isEmpty()) {
            setDefaultVariables();
        }

        updateVariableViews();
    }

    boolean longClickSelectVariable(View v) {
        onVariableLongClickView = v;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose variable");

        builder.setItems(this.getResources().getStringArray(R.array.variables), (dialog, which) -> {
            int frameLayoutID = onVariableLongClickView.getId();
            int variableID = which;

            Variable variable = periodic.variablesConfig.get(frameLayoutID);
            variable.dataType = Variable.DataType.fromInteger(variableID);
            periodic.variablesConfig.put(frameLayoutID, variable);

            Gson gson = new Gson();
            String hashMapString = gson.toJson(periodic.variablesConfig);

            mPreferences.edit().putString("VARIABLES", hashMapString).apply();

            updateVariableViews();
        });

        AlertDialog dialog = builder.create();
        dialog.show();

        return false;
    }

    void setDefaultVariables() {
        periodic.variablesConfig.put(findViewById(R.id.fl1).getId(),
                new Variable(findViewById(R.id.fl1TV).getId(),
                        findViewById(R.id.fl1ValueTV).getId(),
                        Variable.DataType.speed)
        );

        periodic.variablesConfig.put(findViewById(R.id.fl31).getId(),
                new Variable(findViewById(R.id.fl31TV).getId(),
                        findViewById(R.id.fl31ValueTV).getId(),
                        Variable.DataType.humanPower)
        );

        periodic.variablesConfig.put(findViewById(R.id.fl32).getId(),
                new Variable(findViewById(R.id.fl32TV).getId(),
                        findViewById(R.id.fl32ValueTV).getId(),
                        Variable.DataType.motorCurrent)
        );

        periodic.variablesConfig.put(findViewById(R.id.fl41).getId(),
                new Variable(findViewById(R.id.fl41TV).getId(),
                        findViewById(R.id.fl41ValueTV).getId(),
                        Variable.DataType.heartRate)
        );

        periodic.variablesConfig.put(findViewById(R.id.fl42).getId(),
                new Variable(findViewById(R.id.fl42TV).getId(),
                        findViewById(R.id.fl42ValueTV).getId(),
                        Variable.DataType.pedalCadence)
        );
    }

    @Override
    public void onResume() {
        super.onResume();
        updateView();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, mIntentFilter);
        hrSensorService.setOnHRNotificationReceivedListener(() -> {
            updateView();
            adjustAssistLevel();
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
    }

    private final BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "onReceive " + intent.getAction());
            if (intent.getAction() == null)
                return;
            switch (intent.getAction()) {
                case TSDZBTService.TSDZ_PERIODIC_WRITE_BROADCAST:
                    updateView();
                    break;
            }
        }
    };

    void updateView() {
        mBatterySOCTV.setText(String.valueOf((int) periodic.batterySOC));
        mHRSensorBatteryTV.setText(String.valueOf(hrSensorService.batteryLevel));

        if ((periodic.braking == 1) && (periodic.light == 1))
            mBrakeLightsTV.setText("B L");
        else if (periodic.braking == 1)
            mBrakeLightsTV.setText("B");
        else if (periodic.light == 1)
            mBrakeLightsTV.setText("L");
        else
            mBrakeLightsTV.setText("");

        mAssistLevelValueTV.setText(String.valueOf(periodic.assistLevel));

        updateVariableViews();
    }

    void updateVariableViews() {
        TextView tv;

        for (int frameLayoutID : periodic.variablesConfig.keySet()) {
            Variable variable = periodic.variablesConfig.get(frameLayoutID);

            tv = (TextView) findViewById(variable.labelTV);
            tv.setText(variable.dataType.getName());
            tv = (TextView) findViewById(variable.valueTV);

            switch (variable.dataType) {
                case batteryVoltage:
                    tv.setText(String.valueOf(periodic.batteryVoltage / 10));
                    break;

                case batteryCurrent:
                    tv.setText(String.valueOf(periodic.batteryCurrent));
                    break;

                case batterySOC:
                    tv.setText(String.valueOf(periodic.batterySOC));
                    break;

                case batteryUsedEnergy:
                    tv.setText(String.valueOf(periodic.wattsHour));
                    break;

                case batteryADCCurrent:
                    tv.setText(String.valueOf(periodic.ADCBatteryCurrent));
                    break;

                case motorCurrent:
                    tv.setText(String.valueOf(periodic.motorCurrent));
                    break;

                case motorTemperature:
                    tv.setText(String.valueOf(periodic.motorTemperature));
                    break;

                case motorSpeed:
                    tv.setText(String.valueOf(periodic.motorSpeedERPS));
                    break;

                case speed:
                    tv.setText(String.valueOf(periodic.wheelSpeed));
                    break;

                case hallSensors:
                    tv.setText(String.valueOf(periodic.motorHallSensors));
                    break;

                case pedalSide:
                    if (periodic.PASPedalRight == 1)
                        tv.setText("right");
                    else
                        tv.setText("left");
                    break;

                case throttle:
                    tv.setText(String.valueOf(periodic.throttle));
                    break;

                case throttleADC:
                    tv.setText(String.valueOf(periodic.ADCThrottle));
                    break;

                case torqueSensorADC:
                    tv.setText(String.valueOf(periodic.ADCPedalTorqueSensor));
                    break;

                case pedalWeight:
                    tv.setText(String.valueOf(periodic.pedalWeight));
                    break;

                case pedalWeightWithOffset:
                    tv.setText(String.valueOf(periodic.pedalWeightWithOffset));
                    break;

                case pedalCadence:
                    tv.setText(String.valueOf(periodic.pedalCadence));
                    break;

                case dutyCyle:
                    tv.setText(String.valueOf(periodic.dutyCycle));
                    break;

                case focAngle:
                    tv.setText(String.valueOf(periodic.FOCAngle));
                    break;

                case humanPower:
                    tv.setText(String.valueOf(periodic.humanPedalPower));
                    break;

                case odometer:
                    tv.setText(String.valueOf(periodic.odometer));
                    break;
                case heartRate:
                    tv.setText(String.valueOf(periodic.heartRate));
                    break;
            }
        }
    }

    private void adjustAssistLevel() {
        //TODO Andro
        //korisiti varijablu periodic.heartRate
    }
}