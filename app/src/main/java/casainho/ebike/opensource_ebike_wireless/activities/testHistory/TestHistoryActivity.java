package casainho.ebike.opensource_ebike_wireless.activities.testHistory;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Window;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import casainho.ebike.opensource_ebike_wireless.MyApp;
import casainho.ebike.opensource_ebike_wireless.R;
import casainho.ebike.opensource_ebike_wireless.data.InitialTestData;

import static casainho.ebike.opensource_ebike_wireless.activities.InitialTestActivity.TEST_RESULTS;

public class TestHistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            this.getSupportActionBar().hide();
        }
        catch (NullPointerException e){}
        setContentView(R.layout.activity_test_history);

        Gson gson = new Gson();
        String resultsHistoryStr = MyApp.getPreferences().getString(TEST_RESULTS, null);
        if(resultsHistoryStr == null) {
            return;
        } else {
            List<InitialTestData> resultsHistory = Arrays.asList(gson.fromJson(resultsHistoryStr, InitialTestData[].class));

            RecyclerView resultsRecycleView = findViewById(R.id.results_recycler_view);
            resultsRecycleView.setAdapter(new ResultsRecyclerAdapter(resultsHistory));
            resultsRecycleView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            resultsRecycleView.setHasFixedSize(true);
        }

    }
}